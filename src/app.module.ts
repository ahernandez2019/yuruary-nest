import { Module } from "@nestjs/common";
import { TypeOrmModule } from "@nestjs/typeorm";
import ProjectModules from "./module";
import { ConfigModule } from "@nestjs/config";

@Module({
  imports: [TypeOrmModule.forRoot(), ConfigModule.forRoot(), ...ProjectModules],
})
export class AppModule {}
