import { Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { Repository } from "typeorm";
import { MetodoDePagoEntity } from "src/entity/metodoDePago.entity";

@Injectable()
export class MetodoDePagoService {
  constructor(
    @InjectRepository(MetodoDePagoEntity)
    public paymentMethodsRepository: Repository<MetodoDePagoEntity>,
  ) {}

  async findAll(): Promise<MetodoDePagoEntity[]> {
    return await this.paymentMethodsRepository.find();
  }
}
