import { Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { Repository } from "typeorm";
import { BancoEntity } from "../entity/banco.entity";

@Injectable()
export class BancoService {
  constructor(
    @InjectRepository(BancoEntity)
    public bankRepository: Repository<BancoEntity>,
  ) {}

  async findAll(): Promise<BancoEntity[]> {
    return await this.bankRepository.find();
  }
}
