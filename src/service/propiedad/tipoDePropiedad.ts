import { Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { Repository } from "typeorm";
import { TipoDePropiedadEntity } from "../../entity/propiedad/tipoDePropiedad.entity";

@Injectable()
export class TipoDePropiedadService {
  constructor(
    @InjectRepository(TipoDePropiedadEntity)
    public tiposDePropiedadRepository: Repository<TipoDePropiedadEntity>,
  ) {}

  async listarTiposDePropiedad(): Promise<any> {
    return await this.tiposDePropiedadRepository.find();
  }
}
