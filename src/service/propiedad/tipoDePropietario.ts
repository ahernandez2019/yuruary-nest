import { Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { Repository } from "typeorm";
import { TipoDePropietarioEntity } from "../../entity/propiedad/tipoDePropietario.entity";

@Injectable()
export class TipoDePropietarioService {
  constructor(
    @InjectRepository(TipoDePropietarioEntity)
    public tipoDePropietarioRepository: Repository<TipoDePropietarioEntity>,
  ) {}

  async listarTiposDePropietario(): Promise<any> {
    return await this.tipoDePropietarioRepository.find();
  }
}
