import { Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { Repository } from "typeorm";
import { PropietarioEntity } from "../../entity/propiedad/propietario.entity";

@Injectable()
export class PropietarioService {
  constructor(
    @InjectRepository(PropietarioEntity)
    public readonly propietarioRepository: Repository<PropietarioEntity>,
  ) {}

  async buscar(): Promise<any> {
    return await this.propietarioRepository.find({ relations: ["tipoDepropietario", "banco"] });
  }
}
