import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  ManyToOne,
  CreateDateColumn,
  UpdateDateColumn,
  JoinColumn,
} from "typeorm";

import { RolEntity } from "./rolDeUsuario.entity";

@Entity({ name: "usuario" })
export class UsuarioEntity {
  @PrimaryGeneratedColumn("uuid")
  id: string;

  @Column()
  nombre: string;

  @Column()
  apellido: string;

  @Column()
  cedula: number;

  @Column({ unique: true })
  correo: string;

  @Column({ select: false })
  clave: string;

  @Column({ default: true })
  activo: boolean;

  @CreateDateColumn({ type: "timestamp" })
  creado: Date;

  @UpdateDateColumn({ type: "timestamp" })
  actualizado: Date;

  @Column({ default: false })
  estaEnLinea: boolean;

  @ManyToOne(
    () => RolEntity,
    rol => rol.usuario,
  )
  @JoinColumn()
  rol: RolEntity;
}
