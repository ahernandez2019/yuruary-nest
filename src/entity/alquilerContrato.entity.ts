import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  ManyToOne,
  CreateDateColumn,
  UpdateDateColumn,
  JoinColumn,
  BeforeInsert,
  DeleteDateColumn,
  OneToMany,
  OneToOne,
} from "typeorm";
import { AlquilerMensualidadEntity } from "./alquilerMensualidad.entity";
import { GastosBasicosEntity } from "./gastosBasicos.entity";
import { InquilinoEntity } from "./inquilino.entity";
import { PropiedadEntity } from "./propiedad/propiedad.entity";
import { TipoDeUnidadEntity } from "./tipoDeUnidad.entity";
import { UnidadAlquiladaEntity } from "./unidadAlquilada.entity";

@Entity({ name: "alquilerContrato" })
export class AlquilerContratoEntity {
  @PrimaryGeneratedColumn("uuid")
  id: string;

  @Column({ type: "date" })
  fechaDeEmisionDeContrato: Date;

  @Column({ type: "date" })
  fechaFinDeContratoAlquiler: Date;

  @Column({ type: "date" })
  fechaNotificacionNoProrroga: Date;

  @Column({ type: "date" })
  fechaDeVencimientoDeProrroga: Date;

  @Column()
  estado: string;

  @ManyToOne(
    () => PropiedadEntity,
    propiedad => propiedad.alquilerContrato,
  )
  @JoinColumn()
  propiedad: PropiedadEntity;

  @OneToMany(
    () => GastosBasicosEntity,
    gastosBasicos => gastosBasicos.alquilerContrato,
  )
  @JoinColumn()
  gastosBasicos: GastosBasicosEntity[];

  @OneToOne(type => UnidadAlquiladaEntity)
  @JoinColumn()
  unidadAlquilada: UnidadAlquiladaEntity;

  @ManyToOne(
    () => InquilinoEntity,
    inquilino => inquilino.alquilerContrato,
  )
  @JoinColumn()
  inquilino: InquilinoEntity;

  @OneToMany(
    () => AlquilerMensualidadEntity,
    alquilerMensualidad => alquilerMensualidad.alquilerContrato,
  )
  @JoinColumn()
  alquilerMensualidad: AlquilerMensualidadEntity[];
}
