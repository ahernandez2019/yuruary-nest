import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  ManyToOne,
  CreateDateColumn,
  UpdateDateColumn,
  JoinColumn,
  BeforeInsert,
  DeleteDateColumn,
  OneToMany,
} from "typeorm";
import { AlquilerContratoEntity } from "./alquilerContrato.entity";
import { BancoEntity } from "./banco.entity";
import { BeneficiarioEntity } from "./benificiario.entity";
import { GastosBasicosEntity } from "./gastosBasicos.entity";
import { InquilinoEntity } from "./inquilino.entity";
import { MetodoDePagoEntity } from "./metodoDePago.entity";
import { PropietarioEntity } from "./propiedad/propietario.entity";

@Entity({ name: "pagoAPropietarios" })
export class PagoAPropietariosEntity {
  @PrimaryGeneratedColumn("uuid")
  id: string;

  @Column({ type: "date" })
  fechaDeIngreso: Date;

  @Column()
  montoDeBs: number;

  @Column()
  montoDeEur: number;

  @Column()
  montoDeDoll: number;

  @ManyToOne(
    () => BeneficiarioEntity,
    beneficiario => beneficiario.pagoAPropietarios,
  )
  @JoinColumn()
  beneficiario: BeneficiarioEntity;

  @ManyToOne(
    () => PropietarioEntity,
    propietario => propietario.pagoAPropietario,
  )
  @JoinColumn()
  propietario: PropietarioEntity;

  @ManyToOne(
    () => BancoEntity,
    banco => banco.pagoAPropietario,
  )
  @JoinColumn()
  banco: BancoEntity;

  @ManyToOne(
    () => MetodoDePagoEntity,
    metodoDePago => metodoDePago.pagoAPropietario,
  )
  @JoinColumn()
  metodoDePago: MetodoDePagoEntity;
}
