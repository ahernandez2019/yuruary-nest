import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  ManyToOne,
  CreateDateColumn,
  UpdateDateColumn,
  JoinColumn,
  BeforeInsert,
  DeleteDateColumn,
  OneToMany,
} from "typeorm";
import { BalanceInquilinoEntity } from "./balanceInquilino.entity";
import { BalanceYuruaryEntity } from "./balanceYuruary.entity";
import { PropiedadEntity } from "./propiedad/propiedad.entity";

@Entity({ name: "tipoDeOperacion" })
export class TipoDeOperacionEntity {
  @PrimaryGeneratedColumn("uuid")
  id: string;

  @Column()
  Descripcion: string;

  @OneToMany(
    () => BalanceInquilinoEntity,
    balanceInquilino => balanceInquilino.tipoDeOperacion,
  )
  @JoinColumn()
  balanceInquilino: BalanceInquilinoEntity[];

  @OneToMany(
    () => BalanceYuruaryEntity,
    balanceYuruary => balanceYuruary.tipoDeOperacion,
  )
  @JoinColumn()
  balanceYuruary: BalanceYuruaryEntity[];
}
