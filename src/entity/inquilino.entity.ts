import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  ManyToOne,
  CreateDateColumn,
  UpdateDateColumn,
  JoinColumn,
  BeforeInsert,
  DeleteDateColumn,
  OneToMany,
} from "typeorm";
import { AlquilerContratoEntity } from "./alquilerContrato.entity";
import { BancoEntity } from "./banco.entity";
import { GastosBasicosEntity } from "./gastosBasicos.entity";
import { InquilinoDePagoEntity } from "./inquilinoDePago.entity";
import { PropietarioEntity } from "./propiedad/propietario.entity";

@Entity({ name: "inquilino" })
export class InquilinoEntity {
  @PrimaryGeneratedColumn("uuid")
  id: string;

  @Column()
  rif: string;

  @Column()
  cedula: number;

  @Column()
  nombre: string;

  @Column()
  apellido: string;

  @Column()
  direccion: string;

  @Column()
  numeroDeCuenta: number;

  @Column()
  telefonoPrimario: number;

  @Column()
  telefonoSecundario: number;

  @Column()
  correo: string;

  @ManyToOne(
    () => BancoEntity,
    banco => banco.inquilino,
  )
  @JoinColumn()
  banco: BancoEntity;

  @OneToMany(
    () => AlquilerContratoEntity,
    alquilerContrato => alquilerContrato.inquilino,
  )
  @JoinColumn()
  alquilerContrato: AlquilerContratoEntity[];

  @OneToMany(
    () => InquilinoDePagoEntity,
    inquilinoDePago => inquilinoDePago.inquilino,
  )
  @JoinColumn()
  inquilinoDePago: InquilinoDePagoEntity[];
}
