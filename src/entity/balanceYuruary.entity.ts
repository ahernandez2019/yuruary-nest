import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  ManyToOne,
  CreateDateColumn,
  UpdateDateColumn,
  JoinColumn,
  BeforeInsert,
  DeleteDateColumn,
  OneToMany,
} from "typeorm";
import { AlquilerContratoEntity } from "./alquilerContrato.entity";
import { AlquilerMensualidadEntity } from "./alquilerMensualidad.entity";
import { BancoEntity } from "./banco.entity";
import { GastosBasicosEntity } from "./gastosBasicos.entity";
import { MetodoDePagoEntity } from "./metodoDePago.entity";
import { PropietarioEntity } from "./propiedad/propietario.entity";
import { TipoDeOperacionEntity } from "./tipoDeCredito.entity";

@Entity({ name: "balanceYuruary" })
export class BalanceYuruaryEntity {
  @PrimaryGeneratedColumn("uuid")
  id: string;

  @Column()
  valorDeBs: number;

  @Column()
  valorDeEur: number;

  @Column()
  valorDeDoll: number;

  @Column()
  saldoEnDeBs: number;

  @Column()
  saldoDeBs: number;

  @Column({ type: "date" })
  fecha: Date;

  @ManyToOne(
    () => TipoDeOperacionEntity,
    tipoDeOperacion => tipoDeOperacion.balanceYuruary,
  )
  @JoinColumn()
  tipoDeOperacion: TipoDeOperacionEntity;
}
