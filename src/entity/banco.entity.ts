import { Entity, PrimaryGeneratedColumn, Column, OneToMany, JoinColumn } from "typeorm";
import { BeneficiarioEntity } from "./benificiario.entity";
import { InquilinoEntity } from "./inquilino.entity";
import { InquilinoDePagoEntity } from "./inquilinoDePago.entity";
import { PagoAPropietariosEntity } from "./pagoAPropietarios.entity";
import { PropietarioEntity } from "./propiedad/propietario.entity";

@Entity({ name: "banco" })
export class BancoEntity {
  @PrimaryGeneratedColumn("uuid")
  id: string;

  @Column()
  nombre: string;

  @OneToMany(
    () => PropietarioEntity,
    propietario => propietario.banco,
  )
  @JoinColumn()
  propietario: PropietarioEntity[];

  @OneToMany(
    () => BeneficiarioEntity,
    beneficiario => beneficiario.banco,
  )
  @JoinColumn()
  beneficiario: BeneficiarioEntity[];

  @OneToMany(
    () => InquilinoEntity,
    inquilino => inquilino.banco,
  )
  @JoinColumn()
  inquilino: InquilinoEntity[];

  @OneToMany(
    () => InquilinoDePagoEntity,
    inquilinoDePago => inquilinoDePago.banco,
  )
  @JoinColumn()
  inquilinoDePago: InquilinoDePagoEntity[];

  @OneToMany(
    () => PagoAPropietariosEntity,
    pagoAPropietario => pagoAPropietario.banco,
  )
  @JoinColumn()
  pagoAPropietario: PagoAPropietariosEntity[];
}
