import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  ManyToOne,
  CreateDateColumn,
  UpdateDateColumn,
  JoinColumn,
  BeforeInsert,
  DeleteDateColumn,
  OneToMany,
} from "typeorm";
import { PropietarioEntity } from "./propietario.entity";

@Entity({ name: "tipoDePropietario" })
export class TipoDePropietarioEntity {
  @PrimaryGeneratedColumn("uuid")
  id: string;

  @Column()
  descripcion: string;

  @OneToMany(
    () => PropietarioEntity,
    propietario => propietario.tipoDepropietario,
  )
  @JoinColumn()
  propietario: PropietarioEntity[];
}
