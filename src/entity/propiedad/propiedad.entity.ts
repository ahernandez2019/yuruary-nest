import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  ManyToOne,
  CreateDateColumn,
  UpdateDateColumn,
  JoinColumn,
  BeforeInsert,
  DeleteDateColumn,
  OneToMany,
} from "typeorm";
import { AlquilerContratoEntity } from "../alquilerContrato.entity";
import { PropietarioEntity } from "./propietario.entity";
import { TipoDePropiedadEntity } from "./tipoDePropiedad.entity";
import { TipoDePropietarioEntity } from "./tipoDePropietario.entity";

@Entity({ name: "propiedad" })
export class PropiedadEntity {
  @PrimaryGeneratedColumn("uuid")
  id: string;

  @Column()
  mandato: string;

  @Column()
  direccion: string;

  @Column()
  nombre: string;

  @Column()
  numeroDePisos: number;

  @Column()
  numeroDeUnidades: number;

  @OneToMany(
    () => PropietarioEntity,
    propietario => propietario.propiedad,
  )
  @JoinColumn()
  propietario: PropietarioEntity[];

  @ManyToOne(
    () => TipoDePropiedadEntity,
    tipoDePropiedad => tipoDePropiedad.propiedad,
  )
  @JoinColumn()
  tipoDePropiedad: TipoDePropiedadEntity;

  @OneToMany(
    () => AlquilerContratoEntity,
    alquilerContrato => alquilerContrato.propiedad,
  )
  @JoinColumn()
  alquilerContrato: AlquilerContratoEntity[];
}
