import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  ManyToOne,
  CreateDateColumn,
  UpdateDateColumn,
  JoinColumn,
  BeforeInsert,
  DeleteDateColumn,
  OneToMany,
} from "typeorm";
import { PropiedadEntity } from "./propiedad.entity";

@Entity({ name: "tipoDePropiedad" })
export class TipoDePropiedadEntity {
  @PrimaryGeneratedColumn("uuid")
  id: string;

  @Column()
  descripcion: string;

  @OneToMany(
    () => PropiedadEntity,
    propiedad => propiedad.tipoDePropiedad,
  )
  @JoinColumn()
  propiedad: PropiedadEntity[];
}
