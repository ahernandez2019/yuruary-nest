import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  ManyToOne,
  CreateDateColumn,
  UpdateDateColumn,
  JoinColumn,
  BeforeInsert,
  DeleteDateColumn,
  OneToMany,
} from "typeorm";
import { BancoEntity } from "../banco.entity";
import { BeneficiarioEntity } from "../benificiario.entity";
import { PagoAPropietariosEntity } from "../pagoAPropietarios.entity";
import { PropiedadEntity } from "./propiedad.entity";
import { TipoDePropietarioEntity } from "./tipoDePropietario.entity";

@Entity({ name: "propietario" })
export class PropietarioEntity {
  @PrimaryGeneratedColumn("uuid")
  id: string;

  @Column()
  rif: string;

  @Column()
  cedula: number;

  @Column()
  nombre: string;

  @Column({ type: "date" })
  fechaDeIngreso: Date;

  @Column()
  numeroDeCuenta: number;

  @Column()
  porcentajeDeParticipacion: number;

  @Column()
  telefonoPrimario: number;

  @Column()
  telefonoSecundario: number;

  @Column()
  correo: string;

  @Column()
  honorariosAdministrativos: number;

  @ManyToOne(
    () => TipoDePropietarioEntity,
    tipoDePropietario => tipoDePropietario.propietario,
  )
  @JoinColumn()
  tipoDepropietario: TipoDePropietarioEntity;

  @ManyToOne(
    () => BancoEntity,
    banco => banco.propietario,
  )
  @JoinColumn()
  banco: BancoEntity;

  @ManyToOne(
    () => PropiedadEntity,
    propiedad => propiedad.propietario,
  )
  @JoinColumn()
  propiedad: PropiedadEntity;

  @OneToMany(
    () => BeneficiarioEntity,
    beneficiario => beneficiario.propietario,
  )
  @JoinColumn()
  beneficiario: BeneficiarioEntity[];

  @OneToMany(
    () => PagoAPropietariosEntity,
    pagoAPropietario => pagoAPropietario.propietario,
  )
  @JoinColumn()
  pagoAPropietario: PagoAPropietariosEntity[];
}
