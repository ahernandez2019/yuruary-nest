import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  ManyToOne,
  CreateDateColumn,
  UpdateDateColumn,
  JoinColumn,
  BeforeInsert,
  DeleteDateColumn,
  OneToMany,
} from "typeorm";
import { GastosBasicosEntity } from "./gastosBasicos.entity";

@Entity({ name: "tipoDeGasto" })
export class TipoDeGastoEntity {
  @PrimaryGeneratedColumn("uuid")
  id: string;

  @Column()
  Descripcion: string;

  @OneToMany(
    () => GastosBasicosEntity,
    gastosBasicos => gastosBasicos.tipoDeGasto,
  )
  @JoinColumn()
  gastosBasicos: GastosBasicosEntity[];
}
