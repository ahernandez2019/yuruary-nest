import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  ManyToOne,
  CreateDateColumn,
  UpdateDateColumn,
  JoinColumn,
  BeforeInsert,
  DeleteDateColumn,
} from "typeorm";
import { AlquilerContratoEntity } from "./alquilerContrato.entity";
import { TipoDeGastoEntity } from "./tipoDeGasto.entity";

@Entity({ name: "gastosBasicos" })
export class GastosBasicosEntity {
  @PrimaryGeneratedColumn("uuid")
  id: string;

  @Column({ type: "date" })
  fecha: Date;

  @Column()
  montoEnBs: number;

  @Column()
  montoEnEur: number;

  @Column()
  montoEnDoll: number;

  @ManyToOne(
    () => AlquilerContratoEntity,
    alquilerContrato => alquilerContrato.gastosBasicos,
  )
  @JoinColumn()
  alquilerContrato: AlquilerContratoEntity;

  @ManyToOne(
    () => TipoDeGastoEntity,
    tipoDeGasto => tipoDeGasto.gastosBasicos,
  )
  @JoinColumn()
  tipoDeGasto: TipoDeGastoEntity;
}
