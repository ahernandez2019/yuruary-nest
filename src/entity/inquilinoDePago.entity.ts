import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  ManyToOne,
  CreateDateColumn,
  UpdateDateColumn,
  JoinColumn,
  BeforeInsert,
  DeleteDateColumn,
  OneToMany,
} from "typeorm";
import { AlquilerContratoEntity } from "./alquilerContrato.entity";
import { BancoEntity } from "./banco.entity";
import { GastosBasicosEntity } from "./gastosBasicos.entity";
import { InquilinoEntity } from "./inquilino.entity";
import { MetodoDePagoEntity } from "./metodoDePago.entity";
import { PropietarioEntity } from "./propiedad/propietario.entity";

@Entity({ name: "inquilinoDePago" })
export class InquilinoDePagoEntity {
  @PrimaryGeneratedColumn("uuid")
  id: string;

  @Column()
  mesPagado: number;

  @Column({ type: "date" })
  fechaDePago: Date;

  @Column()
  montonEnBs: number;

  @Column()
  montoEnEur: number;

  @Column()
  montoEnDoll: number;

  @ManyToOne(
    () => InquilinoEntity,
    inquilino => inquilino.inquilinoDePago,
  )
  @JoinColumn()
  inquilino: InquilinoEntity;

  @ManyToOne(
    () => BancoEntity,
    banco => banco.inquilinoDePago,
  )
  @JoinColumn()
  banco: BancoEntity;

  @ManyToOne(
    () => MetodoDePagoEntity,
    metodoDePago => metodoDePago.inquilinoDePago,
  )
  @JoinColumn()
  metodoDePago: MetodoDePagoEntity;
}
