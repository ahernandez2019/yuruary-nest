import { Entity, PrimaryGeneratedColumn, Column, OneToMany, JoinColumn } from "typeorm";
import { BalanceInquilinoEntity } from "./balanceInquilino.entity";
import { InquilinoDePagoEntity } from "./inquilinoDePago.entity";
import { PagoAPropietariosEntity } from "./pagoAPropietarios.entity";

@Entity({ name: "metodoDePago" })
export class MetodoDePagoEntity {
  @PrimaryGeneratedColumn("uuid")
  id: string;

  @Column()
  descripcion: string;

  @OneToMany(
    () => InquilinoDePagoEntity,
    inquilinoDePago => inquilinoDePago.metodoDePago,
  )
  @JoinColumn()
  inquilinoDePago: InquilinoDePagoEntity[];

  @OneToMany(
    () => PagoAPropietariosEntity,
    pagoAPropietario => pagoAPropietario.metodoDePago,
  )
  @JoinColumn()
  pagoAPropietario: PagoAPropietariosEntity[];
}
