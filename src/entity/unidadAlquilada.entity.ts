import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  ManyToOne,
  CreateDateColumn,
  UpdateDateColumn,
  JoinColumn,
  BeforeInsert,
  DeleteDateColumn,
} from "typeorm";
import { TipoDeUnidadEntity } from "./tipoDeUnidad.entity";

@Entity({ name: "unidadAlquilada" })
export class UnidadAlquiladaEntity {
  @PrimaryGeneratedColumn("uuid")
  id: string;

  @Column()
  indentificador: string;

  @Column()
  estado: string;

  @ManyToOne(
    () => TipoDeUnidadEntity,
    tipoDeUnidad => tipoDeUnidad.unidadAlquilada,
  )
  @JoinColumn()
  tipoDeUnidad: TipoDeUnidadEntity;
}
