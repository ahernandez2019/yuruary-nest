import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  ManyToOne,
  CreateDateColumn,
  UpdateDateColumn,
  JoinColumn,
  BeforeInsert,
  DeleteDateColumn,
  OneToMany,
} from "typeorm";
import { UnidadAlquiladaEntity } from "./unidadAlquilada.entity";

@Entity({ name: "tipoDeUnidad" })
export class TipoDeUnidadEntity {
  @PrimaryGeneratedColumn("uuid")
  id: string;

  @Column()
  descripcion: string;

  @OneToMany(
    () => UnidadAlquiladaEntity,
    unidadAlquilada => unidadAlquilada.tipoDeUnidad,
  )
  @JoinColumn()
  unidadAlquilada: UnidadAlquiladaEntity[];
}
