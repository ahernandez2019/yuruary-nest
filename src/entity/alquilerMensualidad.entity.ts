import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  ManyToOne,
  CreateDateColumn,
  UpdateDateColumn,
  JoinColumn,
  BeforeInsert,
  DeleteDateColumn,
  OneToMany,
} from "typeorm";
import { AlquilerContratoEntity } from "./alquilerContrato.entity";
import { BalanceInquilinoEntity } from "./balanceInquilino.entity";
import { BancoEntity } from "./banco.entity";
import { GastosBasicosEntity } from "./gastosBasicos.entity";
import { PropietarioEntity } from "./propiedad/propietario.entity";

@Entity({ name: "alquilerMensualidad" })
export class AlquilerMensualidadEntity {
  @PrimaryGeneratedColumn("uuid")
  id: string;

  @Column()
  rif: string;

  @Column({ type: "date" })
  fechaGenerado: Date;

  @Column()
  mesAFacturar: number;

  @Column()
  estado: string;

  @Column()
  montoEnBs: number;

  @Column()
  montoEnEur: number;

  @Column()
  montoEnDoll: number;

  @ManyToOne(
    () => AlquilerContratoEntity,
    alquilerContrato => alquilerContrato.alquilerMensualidad,
  )
  @JoinColumn()
  alquilerContrato: AlquilerContratoEntity;

  @OneToMany(
    () => BalanceInquilinoEntity,
    balanceInquilino => balanceInquilino.alquilerMensualidad,
  )
  @JoinColumn()
  balanceInquilino: BalanceInquilinoEntity[];
}
