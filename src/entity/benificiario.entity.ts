import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  ManyToOne,
  CreateDateColumn,
  UpdateDateColumn,
  JoinColumn,
  BeforeInsert,
  DeleteDateColumn,
  OneToMany,
} from "typeorm";
import { BancoEntity } from "./banco.entity";
import { GastosBasicosEntity } from "./gastosBasicos.entity";
import { PagoAPropietariosEntity } from "./pagoAPropietarios.entity";
import { PropietarioEntity } from "./propiedad/propietario.entity";

@Entity({ name: "beneficiario" })
export class BeneficiarioEntity {
  @PrimaryGeneratedColumn("uuid")
  id: string;

  @Column()
  rif: string;

  @Column()
  cedula: number;

  @Column()
  nombre: string;

  @Column()
  apellido: string;

  @Column()
  direccion: string;

  @Column()
  numeroDeCuenta: number;

  @Column()
  telefonoPrimario: number;

  @Column()
  telefonoSecundario: number;

  @Column()
  correo: string;

  @ManyToOne(
    () => BancoEntity,
    banco => banco.beneficiario,
  )
  @JoinColumn()
  banco: BancoEntity;

  @ManyToOne(
    () => PropietarioEntity,
    propietario => propietario.beneficiario,
  )
  @JoinColumn()
  propietario: PropietarioEntity;

  @OneToMany(
    () => PagoAPropietariosEntity,
    pagoAPropietarios => pagoAPropietarios.beneficiario,
  )
  @JoinColumn()
  pagoAPropietarios: PagoAPropietariosEntity[];
}
