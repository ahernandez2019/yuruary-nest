import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  ManyToOne,
  CreateDateColumn,
  UpdateDateColumn,
  JoinColumn,
  BeforeInsert,
  DeleteDateColumn,
  OneToMany,
} from "typeorm";
import { AlquilerContratoEntity } from "./alquilerContrato.entity";
import { AlquilerMensualidadEntity } from "./alquilerMensualidad.entity";
import { BancoEntity } from "./banco.entity";
import { GastosBasicosEntity } from "./gastosBasicos.entity";
import { MetodoDePagoEntity } from "./metodoDePago.entity";
import { PropietarioEntity } from "./propiedad/propietario.entity";
import { TipoDeOperacionEntity } from "./tipoDeCredito.entity";

@Entity({ name: "balanceIquilino" })
export class BalanceInquilinoEntity {
  @PrimaryGeneratedColumn("uuid")
  id: string;

  @Column()
  valorDeBs: number;

  @Column()
  valorDeEur: number;

  @Column()
  valorDeDoll: number;

  @Column()
  saldoEnDeBs: number;

  @Column()
  saldoDeBs: number;

  @ManyToOne(
    () => AlquilerMensualidadEntity,
    alquilerMensualidad => alquilerMensualidad.balanceInquilino,
  )
  @JoinColumn()
  alquilerMensualidad: AlquilerMensualidadEntity;

  @ManyToOne(
    () => TipoDeOperacionEntity,
    tipoDeOperacion => tipoDeOperacion.balanceInquilino,
  )
  @JoinColumn()
  tipoDeOperacion: TipoDeOperacionEntity;
}
