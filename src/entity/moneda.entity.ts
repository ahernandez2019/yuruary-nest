import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  ManyToOne,
  CreateDateColumn,
  UpdateDateColumn,
  JoinColumn,
  BeforeInsert,
  DeleteDateColumn,
} from "typeorm";

@Entity({ name: "moneda" })
export class MonedaEntity {
  @PrimaryGeneratedColumn("uuid")
  id: string;

  @Column()
  monedaDeOrigen: number;

  @Column()
  monedaDestino: number;

  @Column()
  tasaDeCambios: number;

  @Column({ type: "date" })
  validoDesde: Date;

  @Column({ type: "date" })
  validoHasta: Date;
}
