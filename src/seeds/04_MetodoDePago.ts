import { Factory, Seeder } from "typeorm-seeding";
import { Connection } from "typeorm";
import { MetodoDePagoEntity } from "../entity/metodoDePago.entity";

export default class MetodoDePago implements Seeder {
  public async run(factory: Factory, connection: Connection): Promise<any> {
    await connection
      .createQueryBuilder()
      .insert()
      .into(MetodoDePagoEntity)
      .values([
        { descripcion: "efectivo" },
        { descripcion: "cheque" },
        { descripcion: "transferencia" },
        { descripcion: "deposito" },
      ])
      .execute();
  }
}
