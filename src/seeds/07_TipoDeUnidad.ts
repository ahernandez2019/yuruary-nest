import { Factory, Seeder } from "typeorm-seeding";
import { Connection } from "typeorm";
import { TipoDeUnidadEntity } from "src/entity/tipoDeUnidad.entity";

export default class TipoDeUnidad implements Seeder {
  public async run(factory: Factory, connection: Connection): Promise<any> {
    const tipoDeUnidad = [
      "casa",
      "apartamento",
      "terreno",
      "local",
      "oficina",
      "mezzanina",
      "azotea",
      "sótano",
      "quinta",
      "centro comercial",
    ];

    await connection
      .createQueryBuilder()
      .insert()
      .into(TipoDeUnidadEntity)
      .values(tipoDeUnidad.map(unidad => ({ descripcion: unidad })))
      .execute();
  }
}
