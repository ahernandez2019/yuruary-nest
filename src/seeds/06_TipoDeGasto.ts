import { Factory, Seeder } from "typeorm-seeding";
import { Connection } from "typeorm";
import { MetodoDePagoEntity } from "../entity/metodoDePago.entity";
import { TipoDeOperacionEntity } from "src/entity/tipoDeCredito.entity";
import { TipoDeGastoEntity } from "src/entity/tipoDeGasto.entity";

export default class TipoDeGasto implements Seeder {
  public async run(factory: Factory, connection: Connection): Promise<any> {
    await connection
      .createQueryBuilder()
      .insert()
      .into(TipoDeGastoEntity)
      .values([
        { Descripcion: "Agua" },
        { Descripcion: "Electricidad" },
        { Descripcion: "Internet" },
        { Descripcion: "Mantenimiento" },
      ])
      .execute();
  }
}
