import { Factory, Seeder } from "typeorm-seeding";
import { Connection } from "typeorm";
import { BancoEntity } from "../entity/banco.entity";

export default class Bancos implements Seeder {
  public async run(factory: Factory, connection: Connection): Promise<any> {
    await connection
      .createQueryBuilder()
      .insert()
      .into(BancoEntity)
      .values([
        { nombre: "100% banco" },
        { nombre: "b.o.d" },
        { nombre: "bancamiga" },
        { nombre: "bancaribe" },
        { nombre: "banco activo" },
        { nombre: "banco agrícola de vzla" },
        { nombre: "banco caroní" },
        { nombre: "banco de venezuela" },
        { nombre: "banco del sur" },
        { nombre: "banco del tesoro" },
        { nombre: "banco exterior" },
        { nombre: "banco fondo común" },
        { nombre: "banco mercantil" },
        { nombre: "banco nacional de crédito" },
        { nombre: "banco plaza" },
        { nombre: "banco provincial" },
        { nombre: "bancrecer" },
        { nombre: "banesco" },
        { nombre: "banfanb" },
        { nombre: "bangente" },
        { nombre: "banplus" },
        { nombre: "bicentenario" },
        { nombre: "citibank" },
        { nombre: "mi banco" },
        { nombre: "sofitasa" },
        { nombre: "venezolana de crédito" },
      ])
      .execute();
  }
}
