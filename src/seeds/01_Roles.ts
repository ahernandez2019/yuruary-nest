import { Factory, Seeder } from "typeorm-seeding";
import { Connection } from "typeorm";
import { RolEntity } from "../entity/autenticacion/rolDeUsuario.entity";
import { RolesDeUsuariosEnum } from "../constants/generic.constants";

export default class Roles implements Seeder {
  public async run(factory: Factory, connection: Connection): Promise<any> {
    await connection
      .createQueryBuilder()
      .insert()
      .into(RolEntity)
      .values([
        { descripcion: RolesDeUsuariosEnum.ADMIN },
        { descripcion: RolesDeUsuariosEnum.CAJA },
        { descripcion: RolesDeUsuariosEnum.ANALISTA },
        { descripcion: RolesDeUsuariosEnum.GERENTE_COBRANZA },
        { descripcion: RolesDeUsuariosEnum.COORDINADOR },
        { descripcion: RolesDeUsuariosEnum.GERENTE_GENERAL },
        { descripcion: RolesDeUsuariosEnum.DIRECTOR },
      ])
      .execute();
  }
}
