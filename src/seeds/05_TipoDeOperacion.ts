import { Factory, Seeder } from "typeorm-seeding";
import { Connection } from "typeorm";
import { MetodoDePagoEntity } from "../entity/metodoDePago.entity";
import { TipoDeOperacionEntity } from "src/entity/tipoDeCredito.entity";

export default class TipoDeOperacion implements Seeder {
  public async run(factory: Factory, connection: Connection): Promise<any> {
    await connection
      .createQueryBuilder()
      .insert()
      .into(TipoDeOperacionEntity)
      .values([{ Descripcion: "Debito" }, { Descripcion: "Credito" }])
      .execute();
  }
}
