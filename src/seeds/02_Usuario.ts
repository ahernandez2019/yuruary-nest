import { Factory, Seeder } from "typeorm-seeding";
import { Connection } from "typeorm";
import { UsuarioEntity } from "../entity/autenticacion/usuario.entity";
import { RolEntity } from "../entity/autenticacion/rolDeUsuario.entity";
import { RolesDeUsuariosEnum } from "../constants/generic.constants";
import * as argon2 from "argon2";

export default class Usuario implements Seeder {
  public async run(factory: Factory, connection: Connection): Promise<any> {
    const rol = await connection.manager.findOne(RolEntity, {
      where: {
        descripcion: RolesDeUsuariosEnum.ADMIN,
      },
    });

    await connection
      .createQueryBuilder()
      .insert()
      .into(UsuarioEntity)
      .values([
        {
          nombre: "admin",
          apellido: "admin",
          cedula: 12345678,
          correo: "admin@admin.com",
          clave: await argon2.hash("1234"),
          rol,
        },
      ])
      .execute();
  }
}
