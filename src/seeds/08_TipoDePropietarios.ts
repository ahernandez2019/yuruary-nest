import { Factory, Seeder } from "typeorm-seeding";
import { Connection } from "typeorm";
import { TipoDePropietarioEntity } from "src/entity/propiedad/tipoDePropietario.entity";

export default class TipoDePropietario implements Seeder {
  public async run(factory: Factory, connection: Connection): Promise<any> {
    const tipo = ["natural", "juridico"];

    await connection
      .createQueryBuilder()
      .insert()
      .into(TipoDePropietarioEntity)
      .values(
        tipo.map(propietario => ({
          descripcion: propietario,
        })),
      )
      .execute();
  }
}
