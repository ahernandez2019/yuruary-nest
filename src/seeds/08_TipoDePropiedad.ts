import { Factory, Seeder } from "typeorm-seeding";
import { Connection } from "typeorm";
import { TipoDePropiedadEntity } from "src/entity/propiedad/tipoDePropiedad.entity";

export default class TipoDePropiedad implements Seeder {
  public async run(factory: Factory, connection: Connection): Promise<any> {
    const tiposDePropiedad = ["casa", "quinta", "terreno", "centro comercial", "edificio"];

    await connection
      .createQueryBuilder()
      .insert()
      .into(TipoDePropiedadEntity)
      .values(
        tiposDePropiedad.map(propiedad => ({
          descripcion: propiedad,
        })),
      )
      .execute();
  }
}
