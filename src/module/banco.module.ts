import { Module, MiddlewareConsumer, NestModule } from "@nestjs/common";
import { TypeOrmModule } from "@nestjs/typeorm";
import { BancoEntity } from "../entity/banco.entity";
import { BancoService } from "../service/banco.service";
import { BancoController } from "../controller/banco.controller";
import { AuthMiddleware } from "../middleware/auth.middleware";
import { UsuarioEntity } from "../entity/autenticacion/usuario.entity";
import { UserModule } from "./user.module";
import { UserService } from "../service/autenticacion/user.service";

@Module({
  imports: [TypeOrmModule.forFeature([UsuarioEntity, BancoEntity]), UserModule],
  providers: [UserService, BancoService],
  controllers: [BancoController],
})
export class BancoModule implements NestModule {
  public configure(consumer: MiddlewareConsumer) {
    consumer.apply(AuthMiddleware).forRoutes(BancoController);
  }
}
