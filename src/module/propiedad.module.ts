import { Module, MiddlewareConsumer, NestModule } from "@nestjs/common";
import { TypeOrmModule } from "@nestjs/typeorm";
import { TipoDePropiedadEntity } from "../entity/propiedad/tipoDePropiedad.entity";
import { TipoDePropietarioEntity } from "../entity/propiedad/tipoDePropietario.entity";
import { TipoDePropiedadService } from "../service/propiedad/tipoDePropiedad";
import { PropiedadController } from "../controller/propiedad";
import { AuthMiddleware } from "../middleware/auth.middleware";
import { UsuarioEntity } from "../entity/autenticacion/usuario.entity";
import { UserModule } from "./user.module";
import { UserService } from "../service/autenticacion/user.service";
import { TipoDePropietarioService } from "src/service/propiedad/tipoDePropietario";
import { PropietarioEntity } from "src/entity/propiedad/propietario.entity";
import { PropietarioService } from "src/service/propiedad/propietario";

@Module({
  imports: [
    TypeOrmModule.forFeature([
      UsuarioEntity,
      TipoDePropiedadEntity,
      TipoDePropietarioEntity,
      PropietarioEntity,
    ]),
    UserModule,
  ],
  providers: [UserService, TipoDePropiedadService, TipoDePropietarioService, PropietarioService],
  controllers: [PropiedadController],
})
export class PropiedadModule implements NestModule {
  public configure(consumer: MiddlewareConsumer) {
    consumer.apply(AuthMiddleware).forRoutes(PropiedadController);
  }
}
