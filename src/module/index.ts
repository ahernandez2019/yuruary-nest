import { UserModule } from "./user.module";
import { BancoModule } from "./banco.module";
import { PropiedadModule } from "./propiedad.module";

export default [UserModule, BancoModule, PropiedadModule];
