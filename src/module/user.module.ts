import { MiddlewareConsumer, Module, NestModule, RequestMethod } from "@nestjs/common";
import { TypeOrmModule } from "@nestjs/typeorm";
import { UserService } from "../service/autenticacion/user.service";
import { JwtService } from "../service/autenticacion/jwt.service";
import { UserController } from "../controller/user.controller";
import { UsuarioEntity } from "../entity/autenticacion/usuario.entity";
import { AuthMiddleware } from "../middleware/auth.middleware";
import { RolesService } from "../service/autenticacion/roles";
import { RolEntity } from "../entity/autenticacion/rolDeUsuario.entity";
import { UsersRepository } from "../repositories/autenticacion/user";

@Module({
  imports: [TypeOrmModule.forFeature([UsuarioEntity, RolEntity, UsersRepository])],
  providers: [UserService, JwtService, RolesService],
  controllers: [UserController],
})
export class UserModule implements NestModule {
  public configure(consumer: MiddlewareConsumer) {
    consumer
      .apply(AuthMiddleware)
      .exclude({ path: "/api/usuario/login", method: RequestMethod.POST })
      .forRoutes(UserController);
  }
}
