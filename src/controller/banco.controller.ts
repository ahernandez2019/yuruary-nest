import { Controller, Get, Post } from "@nestjs/common";
import { BancoEntity } from "../entity/banco.entity";
import { BancoService } from "src/service/banco.service";

@Controller("banco")
export class BancoController {
  constructor(public readonly bancoService: BancoService) {}

  @Get()
  getBank(): Promise<BancoEntity[]> {
    return this.bancoService.findAll();
  }
}
