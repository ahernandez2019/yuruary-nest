import { Controller, Get, Post, Body, Query, Headers } from "@nestjs/common";
import { UserService } from "../service/autenticacion/user.service";
import { JwtService } from "../service/autenticacion/jwt.service";
import { RolesService } from "../service/autenticacion/roles";
import { UserLoginIN } from "../interface/user.interface";
import { LoginUserDTO, ListaDeUsuarioDTO } from "../dto/autentication.dto";
import { HttpException } from "@nestjs/common/exceptions/http.exception";
import * as jwt from "jsonwebtoken";
import { SECRET } from "../../config";
import { Logger } from "@nestjs/common";
import { UsuarioEntity } from "../entity/autenticacion/usuario.entity";

@Controller("usuario")
export class UserController {
  private readonly logger = new Logger(UserController.name);

  constructor(
    private readonly userService: UserService,
    private readonly jwtService: JwtService,
    private readonly rolesSrvice: RolesService,
  ) {}

  @Get()
  async obtenerInformacionDelUsuario(@Headers() headers): Promise<any> {
    const authHeaders = headers.authorization;
    const token = (authHeaders as string).split(" ")[1];

    const decoded: any = jwt.verify(token, SECRET);
    return await this.userService.findOneById(decoded.id);
  }

  @Post()
  async guardarYActualizar(@Body() payload: UsuarioEntity) {
    this.userService.saveAndUpdate(payload);
  }

  @Post("login")
  async login(@Body() loginUserData: LoginUserDTO): Promise<UserLoginIN> {
    const _user = await this.jwtService.findOneBycredentials(loginUserData);
    const errors = { message: " User not found" };
    if (!_user) throw new HttpException(errors, 401);
    const token = this.jwtService.generateJWT(_user);

    _user.estaEnLinea = true;
    await this.userService.saveAndUpdate(_user);

    return {
      correo: _user.correo,
      rol: _user.rol.descripcion,
      token,
    };
  }

  @Get("listaDeUsuarios")
  async listaDeUsuario(@Query() params: ListaDeUsuarioDTO) {
    return await this.userService.obtenerDatasetPrincipal(params);
  }

  @Get("roles")
  async rolesUsuario() {
    return await this.rolesSrvice.obtenerRoles();
  }

  @Get("obtenerCantidadPorEstado")
  async obtenerCantidadDeUsuariosActivoseInactivos() {
    return await this.userService.obtenerCantidadDeUsuariosActivoseInactivos();
  }
}
