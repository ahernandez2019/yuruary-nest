import { Controller, Get } from "@nestjs/common";
import { MetodoDePagoEntity } from "src/entity/metodoDePago.entity";
import { MetodoDePagoService } from "src/service/metodoDePago.service";

@Controller("payment-methods")
export class MetodoDePagoController {
  constructor(public readonly paymentMethodsService: MetodoDePagoService) {}

  @Get()
  getPaymentMethods(): Promise<MetodoDePagoEntity[]> {
    return this.paymentMethodsService.findAll();
  }
}
