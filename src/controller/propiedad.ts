import { Body, Controller, Get, Post } from "@nestjs/common";
import { TipoDePropietarioService } from "src/service/propiedad/tipoDePropietario";
import { TipoDePropiedadService } from "../service/propiedad/tipoDePropiedad";
import { PropietarioService } from "../service/propiedad/propietario";

@Controller("propiedad")
export class PropiedadController {
  constructor(
    public readonly tipoDePropiedadService: TipoDePropiedadService,
    public readonly tipoDePropietarioService: TipoDePropietarioService,
    public readonly propietarioService: PropietarioService,
  ) {}

  @Get("listarTiposDePropiedad")
  async listarTiposDePropiedad() {
    return await this.tipoDePropiedadService.listarTiposDePropiedad();
  }

  @Get("listarTiposDePropietario")
  async listarTiposDePropietario() {
    return await this.tipoDePropietarioService.listarTiposDePropietario();
  }

  @Get("buscarPropietario")
  async buscarPropietario() {
    return await this.propietarioService.buscar();
  }

  @Post("guardar")
  async guardar(@Body() payload: any) {
    console.log("%o", payload);
  }
}
