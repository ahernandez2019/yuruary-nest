export interface UserLoginIN {
  correo: string;
  rol: string;
  token: string;
}
