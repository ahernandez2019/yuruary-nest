export enum RolesDeUsuariosEnum {
  ADMIN = "administrador del sistema",
  CAJA = "cajero",
  ANALISTA = "analista",
  GERENTE_COBRANZA = "gerente de cobranza",
  COORDINADOR = "coordinador",
  GERENTE_GENERAL = "gerente general",
  DIRECTOR = "director",
}
